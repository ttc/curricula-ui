# Curricula UI

A react based app to remix training and workshop sessions and is a package
dependency for the curricula-content-scaffold project.

## Installation

```
npm install -S curricula-ui
```

This package defines a few peer dependencies. In order to use the curricula
UI, you have to provide them in your application:

- `react@15.x`
- `react-router@4.x`
- `lodash@4.x`
- `bluebird@3.x`

For local development, simply install those requirements into local space:

```
npm install react@15.x react-router-dom@4.x lodash@4.x bluebird@3.x
```

## Usage

```
import ReactDOM from 'react-dom';
import {BrowserRouter as Router} from 'react-router-dom';
import CurriculaUI from 'curricula-ui';

import 'styles/app.scss';

const App = props => (
  <div>
    <Router basename='/curricula' >
      <CurriculaUI {...props} />
    </Router>
  </div>
);

ReactDOM.render(<App />, document.getElementById('curricula-ui'));
```

Change the `basename` to the URL path, in which you want to mount the app.

In you parent app, provide styles to override the lokk and feel.

**TODO: Add details on how to overwrite styles.**
