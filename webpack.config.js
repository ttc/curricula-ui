const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

module.exports = {
  entry: './src/js/index.js',
  devtool: 'source-map',

  externals: {
    react: {commonjs2: 'react'},
    'react-router-dom': {commonjs2: 'react-router-dom'},
    lodash: {commonjs2: 'lodash'},
    bluebird: {commonjs2: 'bluebird'},
    'prop-types': {commonjs2: 'prop-types'}
  },

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'main.js',
    library: 'CurriculaUI',
    libraryTarget: 'commonjs2',
  },

  resolve: {
    alias: {
      styles: path.resolve(__dirname, 'src/scss')
    },
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'sass-loader']
        })
      },
      {
        test: /\.(woff|woff2|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        include: path.resolve(__dirname, 'node_modules/font-awesome'),
        loader: 'file-loader?name=fonts/[name].[ext]'
      }
    ]
  },

  plugins: [
    new ExtractTextPlugin('main.css'),
    new webpack.SourceMapDevToolPlugin({
      filename: '[name].js.map'
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    new webpack.optimize.UglifyJsPlugin(),
  ],
  node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  },
};
