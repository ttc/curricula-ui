import React from 'react';
import PropTypes from 'prop-types';

export default class IntroSection extends React.Component {
  render() {
    const phrases = this.props.phrases;
    return (
      <div className="intro session container-fluid">
        <h1 className="site-title">{phrases.siteTitle}</h1>
        <p className="subs">{phrases.siteSubTitle}</p>
      </div>
    );
  }
}

IntroSection.displayName = 'Intro Section';

IntroSection.propTypes = {
  phrases: PropTypes.array,
};
