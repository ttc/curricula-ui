import _ from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import locales from '../lib/locales';
import data from '../lib/data';

// const DropDownList = ReactWidgets.DropDownList;

export default class LocaleSwticher extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      locale: data.locales(),
      defaultLocale: data.defaultLocale()};
  }

  componentDidMount() {
    const localesP = locales.toItemsProperty();
    localesP.onValue(
      this.setState({locale: data.locales(), defaultLocale: data.defaultLocale()}));
  }

  render() {
    const defaultLocale = data.defaultLocale();
    const switcher =
      (_.size(this.state.locale) > 1) ?
        <select
          className="language-switcher"
          onChange={this.props.changeLocale}
          defaultValue={this.props.selectedLocale.code ?
            this.props.selectedLocale.code :
            defaultLocale.code}
        >
          <option id="select language"><i className="fa fa-language" /></option>
          {_.map(this.state.locale, (loc) =>
            <option id={loc.code} key={loc.code} value={loc.code}>{loc.name}</option>
          )}
        </select>
        :
        '';

    return (
      <div>
        {switcher}
      </div>
    );
  }
}

LocaleSwticher.displayName = 'LocaleSwticher';

LocaleSwticher.propTypes = {
  locales: PropTypes.array,
  selectedLocale: PropTypes.object,
  changeLocale: PropTypes.func,
};
