import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import Bacon from 'baconjs';
import Promise from 'bluebird';

import data from '../lib/data';
import session from '../lib/session';
import filters from '../lib/filters';
import selections from '../lib/selection';
import locales from '../lib/locales';

import LocaleSwitcher from './LocaleSwitcher';
import IntroSection from './IntroSection';
import Filter from './Filter';
import Session from './Session';
import Selection from './Selection';

export default class Curricula extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showTab: 'sessions',
      startPos: 0,
      fils: {},
      locale: {},
      sessions: [],
      selections: [],
      phrases: [],
      icons: [],
    };
    this.tabSelected = this.tabSelected.bind(this);
    this.affixer = this.affixer.bind(this);
    this.changeLocale = this.changeLocale.bind(this);
    this.resetData = this.resetData.bind(this);
  }

  resetData() {
    Promise.all([data.fetchSettings()])
      .then(() => data.fetchData())
      .then(() => session.reset(data.sessions()))
      .then(() => filters.set())
      .then(() => selections.reset());
  }

  back(e) {
    e.preventDefault();
    this.resetData();
  }

  componentDidMount() {
    const sPos = document.getElementById('filters').style.top;
    this.startPos = sPos || 0;
    this.resetData();
    const showTab = 'sessions';
    const phrases = data.phrases();
    const icons = data.icons();
    const sessionsP = session.toItemsProperty();
    const selectionsP = selections.toItemsProperty();
    const filtersP = filters.toItemsProperty();
    const localesP = locales.toItemsProperty();
    const appState = Bacon.combineTemplate({
      fils: filtersP,
      locale: localesP,
      sessions: sessionsP,
      selections: selectionsP,
    });

    appState.onValue(state => this.setState(state));
    this.setState({
      icons,
      phrases,
      showTab,
    });
    window.onpopstate = this.back;
    window.addEventListener('scroll', this.affixer);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.affixer);
    this.resetData();
  }

  tabSelected(e) {
    const tab = e.target.className.substring(0, e.target.className.indexOf(' '));
    this.setState({showTab: (this.state.showTab === 'sessions' ? tab : 'sessions')});
  }

  affixer(e) {
    // change wrapper div classname to trigger affix
    this.affixWrapper.className = (
      document.body.scrollTop > this.props.startAffixOffset
    ) ?
      'scrolled wrap'
      : 'wrap';

    // trigger scrolling of sidebars when footer scrolls up
    const wrapperHeight = document.getElementById('wrap').clientHeight;
    const startScrollPos = (wrapperHeight - this.props.stopAffixOffset);
    if (document.body.scrollTop > (wrapperHeight - this.props.stopAffixOffset)
    && window.matchMedia('(min-width: 993px)').matches) {
      this.affixWrapper.classList.toggle('end-scroll');
      const toTop = `${(this.startPos +
      (startScrollPos - e.target.documentElement.scrollTop)).toString()}px`;
      document.getElementById('filters').style.top = toTop;
      document.getElementById('selection').style.top = toTop;
    } else if (window.matchMedia('(min-width: 993px)').matches) {
      document.getElementById('filters').style.top = '';
      document.getElementById('selection').style.top = '';
    }
  }

  changeLocale(e) {
    locales.update(e.target.value);
    Promise.all([data.fetchData(e.target.value)])
      .then(() => session.reset(data.sessions()))
      .then(() => filters.set());
  }


  render() {
    const phrases = this.state.phrases;
    const icons = this.state.icons;
    const superSize = _.size(data.sessions());
    const localeCode = this.state.locale;
    return (
      <div className="container-fluid">
        <div className={`main-content ${this.state.showTab}`}>
          <IntroSection phrases={phrases} />
          <div id="language-switcher">
            <LocaleSwitcher
              locales={this.state.locales}
              selectedLocale={this.state.locale}
              changeLocale={(e) => this.changeLocale(e)}
            />
          </div>
          <div id="wrap" className="wrap" ref={(ref) => { this.affixWrapper = ref; } }>
            <Filter
              icons={icons}
              phrases={phrases}
              tabSelected={this.tabSelected}
              showTab={this.state.showTab}
              fils={this.state.fils}
              locale={this.state.locale}
            />
            <Session
              sessions={this.state.sessions}
              icons={icons}
              tabSelected={this.tabSelected}
              superSize={superSize}
              phrases={phrases}
              locale={localeCode.code}
            />
            <Selection
              selections={this.state.selections}
              showTab={this.state.showTab}
              tabSelected={this.tabSelected}
              locale={localeCode.code}
            />
          </div>
        </div>
      </div>
    );
  }
}

Curricula.displayName = 'Curricula';

Curricula.propTypes = {
  settings: PropTypes.object,
  startAffixOffset: PropTypes.number.isRequired,
  stopAffixOffset: PropTypes.number.isRequired,
};
