import React from 'react';
import PropTypes from 'prop-types';
import {Route} from 'react-router-dom';
import Promise from 'bluebird';

import data from './lib/data';
import session from './lib/session';
import filters from './lib/filters';

import Curricula from './components/Curricula';
import SelectionPrint from './components/SelectionPrint';

import '../scss/app.scss';

export default class CurriculaUI extends React.Component {
  componentDidMount() {
    Promise.all([data.fetchSettings()])
      .then(() => data.fetchData())
      .then(() => session.reset(data.sessions()))
      .then(() => filters.set());
  }

  render() {
    return (
      <div>
        <Route exact path='/' component={() => (<Curricula
          settings={this.props.settings}
          startAffixOffset={this.props.startAffixOffset}
          stopAffixOffset={this.props.stopAffixOffset}/>)}
        />
        <Route exact path='/print' component={SelectionPrint} />
      </div>
    );
  }
}

CurriculaUI.displayName = 'CurriculaUI';

CurriculaUI.propTypes = {
  settings: PropTypes.object,
  startAffixOffset: PropTypes.number.isRequired,
  stopAffixOffset: PropTypes.number.isRequired,
};
