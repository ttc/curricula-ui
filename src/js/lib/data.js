/* eslint import/extensions: 0, arrow-body-style:0,
* import/no-webpack-loader-syntax: 0, import/no-unresolved: 0
*/

import _ from 'lodash';
import http from './http';

const dataURL = `${document.location.origin}`;

const settings = {
  dataSource: '',
  dataSourcePath: '',
  locales: [],
  defaultLocale: {},
  startAffixOffset: null,
  stopAffixOffset: null,
  sections: []
};

const fetchSettings = (e) => {
  if (e) {
    _.merge(settings, e[0][0]);
  } else {
    return http.get([`${dataURL}/assets/settings/base-settings.json`])
      .then(s => {
        _.merge(settings, s[0][0]);
      });
  }
};

const sections =
  _.concat(_.map(settings.sectionsObj, 'title'),
    'filters',
    'types',
    'phrases',
    'icons'
  );

const uiElements = [
  'phrases',
  'icons'
];
// We cache our app data in this object. Initialize the data object with empty
// arrays.
const appData = _.reduce(sections, (memo, section) =>
  _.merge(memo, {[section]: []})
  , {});

const appUI = _.reduce(uiElements, (memo, section) =>
  _.merge(memo, {[section]: []})
  , {});

export const fetchData = (locale) => {
  const l = locale || settings.defaultLocale.code;
  const urls = [
    `${dataURL}/assets/data/${l}-data.json`,
    `${dataURL}/assets/data/${l}-ui.json`,
  ];
  return http.get(urls)
    .then(data => {
      _.each(data[0][0], (v, k) => { appData[k] = v; });
      _.merge(appUI, { phrases: data[1][0].phrasing, icons: data[1][0].iconmap });
    });
};

export default {
  fetchData,
  fetchSettings,
  sessions: () => appData.curriculas,
  methodologies: () => appData.methodologies,
  materials: () => appData.materials,
  activities: () => appData.activities,
  appFilters: () => appData.filters,
  appTypes: () => appData.types,
  phrases: () => appUI.phrases,
  icons: () => appUI.icons,
  locales: () => settings.locales,
  defaultLocale: () => settings.defaultLocale,
  startAffixOffset: () => settings.startAffixOffset,
  stopAffixOffset: () => settings.stopAffixOffset,
};
